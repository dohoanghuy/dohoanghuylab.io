---
title: Clean Code - Chapter 1 - Clean Code
date: 2018-11-06 07:30:59
tags: [clean-code, funny]
thumbnail: /2018/11/06/clean-code-chapter1-CleanCode/wtf_per_minute.png
---

# What is clean code?

- elegant
- efficiency
- does one thing well
- read like well-written prose
- smaller is better
- care
