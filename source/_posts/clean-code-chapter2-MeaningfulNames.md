---
title: Clean Code - Chapter 2 - MeaningfulNames
date: 2018-11-06 07:31:57
tags: [clean-code]
thumbnail: /2018/11/06/clean-code-chapter2-MeaningfulNames/meaningful_name.png
---

> If a name requires a comment, then the name dose not reveal its intent

<!-- more -->

## Avoid disinfomation

---

## Make meaningful distinctions

{% asset_img distinction.png %}
It is not sufficient to add number series or noise words, even though the compiler is satified. If names must be different, then they should also mean something different.
Distinguish names in such a way that the reader knows what the differences offer.

---

## Use pronounceable names

---

## Use searchable names

The length of a name should correspond to the size of its scope

---

## Avoid encoding

### Hungarian notation

### Member prefixes

### Interfaces and implementations

---

## Avoid mental mapping

clarity is king

---

## Class name

Should have noun or noun phrase
Should not be a verb

---

## Method name

Should have verb or verb phase
Accessors, mutators, and predicates should be named for their value and prefixed with *get*, *set*, and *is* according to the javabean standard

---

## Don't be cute

{% asset_img don't_cute.png %}

---

## Pick one word per concept

---

## Don't pun

Avoid using the same word for two purposes

---

## Use solution domain names

Remember that the people who read your code will be programmers. So go ahead and use computer science terms, algorithm names, pattern names, and so forth.
Choosing technical names for those thing is usually the most appropriate

---

## Use problem domain names

---

## Add meaningful context

---

## Don't add gratuitous context